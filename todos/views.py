from django.shortcuts import render
from todos.models import TodoList


# Create your views here.
def show_todo_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list_object": todo_list}
    return render(request, "todo_list/list.html", context)
