from django.urls import path
from todos.views import show_todo_list

urlpatterns = [path("todos/", show_todo_list)]
